import torch
import esm
import pandas as pd
import numpy as np

df = pd.read_csv('hackathon.csv', sep='\t')

data = []

amino_acids = set()

for ind, row in df.iterrows():
    if ind > 99:
        break
    seq = row.ab_sequence
    ID = row.ag_ID
    data.append((ID, str(seq)))
    amino_acids = set.union(amino_acids, set(seq))

print(len(data))
print(data[0])
print(amino_acids)
print(len(amino_acids))

# Load ESM-1b model
model, alphabet = esm.pretrained.esm1b_t33_650M_UR50S()
batch_converter = alphabet.get_batch_converter()

batch_labels, batch_strs, batch_tokens = batch_converter(data)

with torch.no_grad():
    token_embeds = model.embed_tokens(batch_tokens).numpy()[:, 1:len(seq) + 1, :]

amino_acid_embeddings = {}

for (_id, _seq), emb in zip(data, token_embeds):
    for _ind, _ch_seq in enumerate(_seq):
        amino_acid_embeddings[_ch_seq] = emb[_ind].flatten().tolist()

import json
json.dump(amino_acid_embeddings, open('embeddings.json', 'w'), indent=4, sort_keys=True)