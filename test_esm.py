import torch
import esm
import pandas as pd
import numpy as np

df = pd.read_csv('hackathon.csv', sep='\t')

data = []

for ind, row in df.iterrows():
    seq = row.ab_sequence
    ID = row.ag_ID
    data.append((ID, str(seq)))

print(len(data))
print(data[0])

# Load ESM-1b model
model, alphabet = esm.pretrained.esm1b_t33_650M_UR50S()
batch_converter = alphabet.get_batch_converter()

batch_labels, batch_strs, batch_tokens = batch_converter(data)

# Extract per-residue representations (on CPU)
with torch.no_grad():
    results = model(batch_tokens, repr_layers=[33], return_contacts=False)

print(results['representations'][33].shape)
token_representations = results["representations"][33]

# Generate per-sequence representations via averaging
# NOTE: token 0 is always a beginning-of-sequence token, so the first residue is token 1.
sequence_representations = []
ag_ids = []

for i, (ID, seq) in enumerate(data):
    sequence_representations.append(token_representations[i, 1 : len(seq) + 1].mean(0).numpy())
    ag_ids.append(ID)

seq_repr = np.array(sequence_representations)
ag_ids = np.array(ag_ids)
print(seq_repr.shape)
print(ag_ids.shape)

np.savez_compressed('seq_repr', seq=seq_repr, ids=ag_ids)
